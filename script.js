/// mengambil pilihan player
function startGame() {
const pilihan = document.querySelectorAll('li img')
pilihan.forEach(function(pil) {
    pil.addEventListener('click', function() {
    const pilihanPlayer = pil.className
    const pilihanComputer = getPilihanComputer()
    const hasil = getHasil(pilihanPlayer, pilihanComputer)
    console.log(pilihanPlayer, pilihanComputer, hasil)

    const hasilGame = document.querySelector('.hasil-game')
    hasilGame.innerHTML = hasil
    })
})

/// mengambil pilihan computer
function getPilihanComputer() {
    /// generate random 
    const comp = Math.random()
    if( comp < 0.34 ) return 'batu'
    if( comp >= 0.34 && comp < 0.67) return 'kertas'
    return 'gunting'
}   

/// rules game
function getHasil(player, comp) {
    if( player == comp ) return 'DRAW'
    if( player == 'batu' ) return ( comp == 'kertas') ? 'COM WIN!' : 'PLAYER 1 WIN!'
    if( player == 'kertas' ) return ( comp == 'batu') ? 'PLAYER 1 WIN!' : 'COM WIN!'
    if( player == 'gunting' ) return ( comp == 'batu') ? 'COM WIN!' : 'PLAYER 1 WIN!'
}
return true
}
startGame()

/// refresh 
document.getElementsByName('refresh').addEventListener('click', () => {
})